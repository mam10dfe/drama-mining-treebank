import nltk
import urllib
import os
import sys
import pickle
import lxml.etree as etree
from nltk.draw.tree import TreeView
from chunker import TagChunker, create_chunker


def parse_as_treebank(urn):

    # Erstellt aus Textstelle unter gegebener CTS-URN pro Satz einen Tree als PNG-Datei
    # sowie für alle Sätze eine Treebank als TXT-Datei.

    urn_cleaned = '_'.join(urn.replace(".","_").split(":")[2:])
    filename_treebank = urn_cleaned + "_treebank.txt"
    path_img = os.path.join("treebank_images", urn_cleaned)

    if os.path.exists(filename_treebank):
        os.remove(filename_treebank)

    if not os.path.isdir(path_img):
        os.makedirs(path_img)


    # Lade auf conll2000 trainierten Chunker (siehe "chunker.py")

    try:
        with open("chunker.pkl", "rb") as f:
            chunker = pickle.load(f)
    except:
        print("Training chunker...")
        chunker = create_chunker()
    

    # Abfragen der Textstelle und Extraktion der Textstellen aus der CTS-XML

    namespaceserver_url = "http://cts.informatik.uni-leipzig.de/nsresolver/?ns="
    namespace = urn.split(":")[2] # urn:cts:pbc 
    url = namespaceserver_url + namespace

    with urllib.request.urlopen(url) as response:
        server_url = response.read().decode('utf-8')
    
    final_url = server_url + "?request=GetPassage&urn=" + urn # ZWISCHENERGEBNIS: URL fuer CTS Request

    with urllib.request.urlopen(final_url) as response:
        root = etree.fromstring(response.read())

    namespaces = {'ns': root.xpath('namespace-uri(.)') }

    text = ''
    content = root.xpath('//ns:content',namespaces=namespaces)

    if len(content) == 0:
        content = root.xpath('//ns:passage',namespaces=namespaces)

    if len(content) == 0:
        print("No text passages found.")
        return

    for child in content:
        text = ' '.join([text] + list(child.itertext()))

    
    # Tokenisierung, POS-Tagging, Chunking

    sentences = nltk.sent_tokenize(text)
    sent_idx = 0

    print("Start generating treebank... Number of sentences in text passage: {}".format(len(sentences)))

    for sentence in sentences:
        
        tokens = nltk.word_tokenize(sentence)
        tagged = nltk.pos_tag(tokens)
        tree = chunker.parse(tagged)
        
        # Speichern in der Treebank-TXT-Datei

        with open(filename_treebank, "a") as outfile:
            outfile.write(str(tree))
            outfile.write("\n")

        # Speichern als PostScript
        
        filename_img = urn_cleaned + str("_{:06d}".format(sent_idx))
        filename_img = os.path.join(path_img, filename_img)
        sent_idx += 1
        
        TreeView(tree)._cframe.print_to_file(filename_img + ".ps")
        
        # Umwandeln zu PNG

        try:
            os.system('convert ' + filename_img + ".ps" + ' ' + filename_img + ".png") 
            os.system('rm ' + filename_img + ".ps")
        except:
            pass

    return



def main():
    if len(sys.argv) < 2:
        print("Please set the CTS URN of the text passage you want to visualize.")
        return
    else:
        urn = sys.argv[1]
        if not urn.startswith("urn:cts:"):
            print("Please set the CTS URN of the text passage you want to visualize.")
            return
        
    parse_as_treebank(urn)

    return



if __name__ == '__main__':
    main()