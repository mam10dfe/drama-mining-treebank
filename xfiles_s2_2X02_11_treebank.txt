(S
  (NP The/DT foreman/NN)
  (VP is/VBZ taking/VBG)
  (NP Mulder/NNP)
  (PP on/IN)
  (NP a/DT grand/JJ tour/NN)
  (PP of/IN)
  (NP the/DT facility/NN)
  ./.)
(S
  (NP City/NNP)
  (VP runs/VBZ)
  (PP on/IN)
  (NP several/JJ different/JJ systems/NNS)
  .../:
  (NP some/DT new/JJ)
  ,/,
  (NP some/DT built/VBN)
  (PP around/IN)
  (NP the/DT turn/NN)
  (PP of/IN)
  (NP the/DT century/NN)
  ./.)
(S
  Almost/RB
  as/RB
  (NP old/JJ)
  (PP as/IN)
  (NP Charlie/NNP)
  here/RB
  ./.)
(S
  (NP He/PRP)
  (VP points/VBZ to/TO)
  (NP an/DT older/JJR man/NN walking/NN)
  (PP by/IN)
  ,/,
  (VP wearing/VBG)
  (NP a/DT hard-hat/JJ)
  ,/,
  (VP writing/VBG)
  (NP something/NN)
  down/RP
  (PP on/IN)
  (NP a/DT clipboard/NN)
  ./.)
(S (VP Is/VBZ n't/RB) that/RB (NP right/JJ) ,/, (NP Charlie/NNP) ?/.)
(S Oh/UH ,/, yes/UH ,/, (NP sir/NN) ./.)
(S
  (NP Charlie/NNP)
  (VP keeps/VBZ)
  (NP walking/NN)
  ,/,
  (PP as/IN)
  (NP does/VBZ Mulder/NNP)
  and/CC
  (NP the/DT foreman/NN)
  ./.)
(S
  (NP They/PRP)
  (VP walk/VBP)
  (PP over/IN)
  (PP to/TO)
  (NP an/DT encased/JJ map/NN)
  ./.)
(S
  (NP What/WP part/NN)
  (PP of/IN)
  (NP the/DT sewer/NN)
  (VP was/VBD)
  (NP I/PRP)
  (PP in/IN)
  ?/.)
(S
  (NP That/DT)
  (VP 'd/MD be/VB)
  (NP one/CD)
  (PP of/IN)
  (NP the/DT older/JJR sections/NNS)
  ,/,
  (NP big/JJ eight-foot/NN tunnels/NNS)
  ./.)
(S
  (NP They/PRP)
  (VP look/VBP)
  more/RBR
  (PP like/IN)
  (NP a/DT catacomb/NN)
  (PP than/IN)
  (NP a/DT sewer/JJR line/NN)
  ./.)
(S
  (NP Yup/NN)
  ,/,
  (NP all/PDT the/DT new/JJ sections/NNS)
  (VP are/VBP)
  (NP concrete/JJ pipe/NN)
  ,/,
  not/RB
  much/RB
  (PP of/IN)
  (NP it/PRP bigger/JJR)
  (PP than/IN)
  (NP twenty-four/JJ inches/NNS)
  ./.)
(S
  And/CC
  (NP all/PDT the/DT sewage/NN)
  (VP comes/VBZ)
  (PP through/IN)
  (NP this/DT plant/NN)
  ?/.)
(S
  (NP Five-hundred-and-sixty/JJ thousand/CD people/NNS)
  (NP a/DT day/NN)
  (VP call/VB)
  (NP my/PRP$ office/NN)
  (PP on/IN)
  (NP the/DT porcelain/NN telephone/NN)
  ./.)
(S
  (NP Mulder/NNP smiles/NNS)
  and/CC
  (VP pulls/VBZ)
  out/RP
  (NP the/DT encased/JJ flukeworm/NN)
  ./.)
(S
  Have/VBP
  (NP you/PRP)
  ever/RB
  (VP seen/VBN)
  (NP one/CD)
  (PP of/IN)
  (NP these/DT)
  ?/.)
(S
  (NP Looks/NNS)
  (PP like/IN)
  (NP a/DT big/JJ)
  ,/,
  (NP old/JJ worm/NN)
  ./.)
(S (NP It/PRP) (VP 's/VBZ called/VBN) (NP a/DT fluke/NN) ./.)
(S
  (NP It/PRP)
  (VP came/VBD)
  (PP from/IN)
  (NP the/DT body/NN)
  (NP they/PRP)
  (VP pulled/VBD out/RB)
  (PP of/IN)
  (NP the/DT sewer/NN)
  ./.)
(S Would/MD n't/RB (VP surprise/VB) (NP me/PRP) ./.)
(S
  (NP No/DT telling/VBG)
  (NP what/WP)
  (VP 's/VBZ been/VBN breeding/VBG)
  down/RB
  there/RB
  (PP in/IN)
  (NP the/DT last/JJ hundred/CD years/NNS)
  ./.)
(S
  (NP Downstairs/NNP)
  ,/,
  (NP the/DT water/NN)
  (VP is/VBZ)
  (NP loud/JJ)
  (PP as/IN)
  (NP it/PRP)
  (VP rumbles/VBZ)
  (PP through/IN)
  (PP as/IN)
  (NP Charlie/NNP)
  (VP stands/VBZ)
  (PP on/IN)
  (NP a/DT plank/NN)
  ,/,
  (VP smoking/VBG)
  (NP a/DT cigarette/NN)
  ./.)
(S
  (NP He/PRP)
  (VP hears/VBZ)
  (NP a/DT rumbling/NN)
  (PP in/IN)
  (NP the/DT water/NN underneath/NN)
  (NP him/PRP)
  and/CC
  (NP sees/NNS something/NN)
  (VP swim/VBP)
  (PP beneath/IN)
  (NP him/PRP)
  ./.)
(S
  (NP Charlie/NNP)
  (VP runs/VBZ)
  (PP over/IN)
  (PP to/TO)
  (NP a/DT pole/NN)
  (PP with/IN)
  (NP a/DT built-in/JJ telephone/NN)
  ./.)
(S
  (NP Upstairs/NNS)
  ,/,
  (NP the/DT foreman/NN)
  (VP picks/VBZ)
  up/RP
  (NP the/DT phone/NN)
  ,/,
  (NP which/WDT)
  (VP has/VBZ)
  (NP a/DT bright/JJ red/JJ light/NN)
  (VP flashing/VBG)
  ./.)
(S (NP Ray/NNP) here/RB ./.)
(S (NP Some/DT talking/VBG) (VP can/MD be/VB heard/VBN) ./.)
(S Yeah/UH ./.)
(S (NP Charlie/NNP) ,/, (VP slow/VB) down/RP ./.)
(S (NP Mulder/NNP) (VP looks/VBZ) over/RP ./.)
(S
  (NP Downstairs/NNS)
  ,/,
  (NP they/PRP)
  (VP run/VBP)
  (PP over/IN)
  (PP to/TO)
  (NP Charlie/NNP)
  ,/,
  (NP who/WP)
  (VP is/VBZ standing/VBG)
  (PP at/IN)
  (NP a/DT system/NN control/NN)
  ./.)
(S (NP I/PRP) (VP 'm/VBP backflushing/VBG) (NP the/DT system/NN) ./.)
(S (NP It/PRP) (VP 's/VBZ) (PP in/IN) (NP there/RB) ./.)
(S (NP What/WP) (VP is/VBZ) (NP it/PRP) ?/.)
(S (NP I/PRP) (VP do/VBP n't/RB know/VB) ./.)
(S
  (NP It/PRP)
  just/RB
  (VP swam/VB)
  (NP right/JJ)
  (PP past/IN)
  (NP me/PRP)
  ./.)
(S (NP He/PRP) (VP flips/VBZ) (NP a/DT switch/NN) ./.)
(S (NP There/EX) (NP it/PRP) (VP is/VBZ) ./.)
(S
  (NP They/PRP)
  (VP walk/VBP)
  (PP over/IN)
  (PP to/TO)
  (NP a/DT yellow-tube/JJ pipe/NN)
  ./.)
(S
  (NP Charlie/NNP)
  (VP shines/VBZ)
  (NP a/DT flashlight/NN)
  (PP on/IN)
  (NP the/DT thing/NN)
  (NP 's/POS face/NN)
  ./.)
(S
  (NP It/PRP)
  (VP 's/VBZ)
  (NP the/DT Flukeman/NNP)
  ,/,
  (NP a/DT giant/JJ flukeworm/NN)
  (NP that/WDT)
  (VP has/VBZ)
  (NP human/JJ qualities/NNS)
  ./.)
(S (NP The/DT scolex/NN) (VP is/VBZ highly/RB) (NP evident/JJ) ./.)
(S
  (NP Ray/NNP)
  and/CC
  (NP Mulder/NNP)
  (VP look/VBP)
  (PP at/IN)
  (NP each/DT other/JJ)
  ,/,
  then/RB
  back/RB
  (PP at/IN)
  (NP the/DT monster/NN)
  ./.)
