import pickle
import nltk
from nltk.chunk import ChunkParserI, accuracy
from nltk.chunk.util import tree2conlltags, conlltags2tree
from nltk.tag import UnigramTagger, BigramTagger
from nltk.corpus import conll2000


# Trainieren eines Chunkers mit Daten aus conll2000. 
# Quelle: "Python 3 Text Processing with NLTK 3 Cookbook" 
#
# Exportiert nach "chunker.pkl"


def backoff_tagger(train_sents, tagger_classes, backoff=None):
    for cls in tagger_classes:
        backoff = cls(train_sents, backoff=backoff)
    return backoff

def conll_tag_chunks(chunk_sents):
    tagged_sents = [tree2conlltags(tree) for tree in chunk_sents]
    return [[(t, c) for (w, t, c) in sent] for sent in tagged_sents]


class TagChunker(ChunkParserI):

    def __init__(self, train_chunks, tagger_classes=[UnigramTagger, BigramTagger]):
        train_sents = conll_tag_chunks(train_chunks)
        self.tagger = backoff_tagger(train_sents, tagger_classes)
     
    def parse(self, tagged_sent):
        if not tagged_sent: return None
        (words, tags) = zip(*tagged_sent)
        chunks = self.tagger.tag(tags)
        wtc = zip(words, chunks)
        return conlltags2tree([(w,t,c) for (w,(t,c)) in wtc])
    

def create_chunker():
    conll_train = conll2000.chunked_sents('train.txt')
    conll_test = conll2000.chunked_sents('test.txt')
    chunker = TagChunker(conll_train)

    print("Chunker accuracy: {0:.2f}%".format((100 * accuracy(chunker, conll_test))))

    with open("chunker.pkl", "wb") as outfile:
        pickle.dump(chunker, outfile)

    return chunker


if __name__ == '__main__':
    create_chunker()


