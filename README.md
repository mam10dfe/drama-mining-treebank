# Drama Mining Treebank

Hausaufgabe 1, 19.4.2019  
Visualisierung von Textstellen, mit CTS-URN Schnittstelle

Ausführen im Terminal mit z.B.  
```
python treebank.py urn:cts:xfiles:s2.2X02:10
```


oder in Python mit  
```
from treebank import parse_as_treebank

parse_as_treebank("urn:cts:xfiles:s2.2X02:10")
```